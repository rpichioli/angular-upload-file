import { Injectable } from '@angular/core';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators'
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class UploadService {
// Default url
private url = 'http://localhost:3000/upload';

// Constructor
constructor(
  private http: HttpClient,
  private _snackBar: MatSnackBar
) { }

/**
 * @description Send file to back-end
 * @param file File
 * @returns Observable<any>
 */
upload(file) {
  return this.http.post<any>(this.url, file, {  reportProgress: true, observe: 'events' })
    .pipe(
      map((event) => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            // Uploading progress
            const progress = Math.round(100 * event.loaded / event.total);
            return { status: 'progress', progress: progress };
          case HttpEventType.Response:
            // SnackBar succees
            this._snackBar.open('Send', '', {
              duration: 3000,
              horizontalPosition: 'end',
              panelClass: ['snackbar-succees']
            });
            return { status: 'done', response: event.body };
          default:
            return 
        }
      }),
      catchError(
        this.errorHandler('Error', [])
      )
    );
}

/**
 * @description Handle error
 * @param message Message that will be displayed to the user 
 * @param result Default error
 * @returns Observable<T>
 */
private errorHandler<T>(message: string, result: T): any {
  return (error: any): Observable<any> => {
    // Console error
    console.error(`${message}: ${error.message}`);
    // SnackBar error
    this._snackBar.open(message, '', {
      duration: 3000,
      horizontalPosition: 'end',
      panelClass: ['snackbar-error']
    });
    // Return error
    return of({ status: 'error', response: result as T });
  };
}
}
